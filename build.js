const babel                = require('babel-core')
const mkdir                = require('mkdirp')
const { writeFile }        = require('fs')
const { join,
        resolve,
        dirname,
        relative } = require('path')

const destinationPath = resolve(__dirname, 'dist')
const sourcePath = resolve(__dirname, 'src')

const files = [
  './index.js',
  './components/Checkbox.js',
  './components/Radio.js',
]

const log = (...msg) =>
  console.log(...msg) // eslint-disable-line no-console


function write (path, content) {
  const dirPath = dirname(path)

  return new Promise( (resolve, reject) =>
    mkdir(dirPath, (err) => {
      err ? reject(err) : log("Created directory", dirPath)
      writeFile(path, content, (err) => err
        ? reject(err)
        : log("Successfully wrote", path)
      )
    })
  )
}

const transpile = (file) =>
  babel.transformFileSync(join(sourcePath, file))

function processResult (result) {
  const filePath   = relative(sourcePath, result.options.filename)
  const contents   = result.code
  const outputPath = resolve(destinationPath, filePath)
  return write(outputPath, contents)
}

const compile = (...entries) =>
  Promise.all(entries
    .map(transpile)
    .map(processResult)
  )

compile(...files)
