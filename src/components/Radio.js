// @flow
import React, { PureComponent } from 'react'
import assert from 'assert'
import prop from 'prop-types'

const propTypes = {
  name:      prop.string,
  value:     prop.bool,
  checked:   prop.bool,
  disabled:  prop.bool,
  onChange:  prop.func.isRequired,
  className: prop.string,

  width:          prop.oneOf([ 'string', 'number' ]),
  color:          prop.string,
  disabledColor:  prop.string,
  highlightColor: prop.string,
}

const defaultProps = {
  size:  1,
  value: false,
  color: 'currentColor',
  disabledColor:  '#757082',
  highlightColor: '#4a4655',
}

const f1 = 2.76243
const f2 = 2.23757


export default class Radio extends PureComponent {

  static PATH = `m2,6.64029c0,-${f1} ${f2},-5 5,-5c${f1},0 5,${f2} 5,5c0,${f1} -${f2},5 -5,5c-${f1},0 -5,-${f2} -5,-5z`
  static propTypes = propTypes
  static defaultProps = defaultProps

  constructor () {
    super(...arguments)
    assert(
      typeof this.props.onChange === 'function',
      `Checkbox must have an onChange prop of type function.`
    )
  }

  toggle = () => this.props.onChange(!this.checked)

  render () {
    const disabled       = this.disabled
    const checked        = this.checked

    const color          = this.props.color
    const disabledColor  = this.props.disabledColor
    const highlightColor = this.props.highlightColor

    const radioStyle = Object.assign({}, style.checkbox, {
      fill:   !checked ? 'transparent' : disabled ? disabledColor : highlightColor,
      stroke: disabled ? disabledColor : checked ? highlightColor : color,
    })

    const svgStyle = Object.assign({}, style.svg, {
      width: this.size,
    })

    return <label
      style={ style.container }
      className={ this.props.className }>

      <input
        type='radio'
        name={ this.props.name }
        style={ style.input }
        checked={ checked }
        disabled={ disabled }
        onChange={ this.toggle }
      />

      <svg
        className='checkbox sprite'
        style={ svgStyle }
        viewBox='0 0 14 14'>
        <path style={ radioStyle } d={ Radio.PATH } />
      </svg>

    </label>
  }

  get checked () {
    return bool('checked' in this.props
      ? this.props.checked
      : this.props.value)
  }

  get disabled () {
    return this.props.disabled ? 'disabled' : false
  }

  get size () {
    if (typeof this.props.size === 'number')
      return `${this.props.size}em`

    // flow-ignore
    return this.props.size
  }

}

const style = {

  input: {
    display: 'none',
    opacity: 0,
    height:  0,
  },

  container: {
    maxHeight: 'none',
    maxWidth:  'none',
    display:   'block',
    cursor:    'pointer',
    height:    'auto',
    width:     'auto',
    margin:    0,
    padding:   0,
  },

  svg: {
    height:   'auto',
    display:  'block',
    maxWidth: 'none',
  },

  checkbox: {
    strokeWidth:    1,
    strokeLinejoin: 'round',
    vectorEffect:   'non-scaling-stroke',
    transition:     'all .3s',
  },
}

const bool = (val) =>
  val ? true : false
