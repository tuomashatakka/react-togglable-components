// @flow
import React, { PureComponent } from 'react'
import assert from 'assert'
import prop from 'prop-types'

const propTypes = {
  name:      prop.string,
  value:     prop.bool,
  checked:   prop.bool,
  disabled:  prop.bool,
  onChange:  prop.func.isRequired,
  className: prop.string,
  children:  prop.children,

  width:          prop.oneOf([ 'string', 'number' ]),
  color:          prop.string,
  highlightColor: prop.string,
}

const defaultProps = {
  value: false,
  width: '1em',
  color: 'currentColor',
  highlightColor: '#4a4655',
}


export default class Checkbox extends PureComponent {

  static PATH = 'm 2 2 h 9 c 1 0 1 1 1 1 v 9 c 0 1 -1 1 -1 1 h -9 c -1 0 -1 -1 -1 -1 v -9 c 0 -1 1 -1 1 -1 z'
  static propTypes = propTypes
  static defaultProps = defaultProps

  constructor () {
    super(...arguments)
    assert(
      typeof this.props.onChange === 'function',
      `Checkbox must have an onChange prop of type function.`
    )
  }

  get checked () {
    return bool('checked' in this.props
      ? this.props.checked
      : this.props.value)
  }

  toggle = () => this.props.onChange(!this.checked)

  render () {
    const color          = this.props.color
    const highlightColor = this.props.highlightColor
    const checked        = this.checked

    const checkboxStyle  = Object.assign({}, style.checkbox, {
      stroke: checked ? highlightColor : color,
    })

    const svgStyle = Object.assign({}, style.svg, {
      width: this.props.width,
    })

    return <label
      style={ style.container }
      className={ this.props.className }>

      <input
        type='checkbox'
        name={ this.props.name }
        style={ style.input }
        checked={ checked }
        onChange={ this.toggle }
      />

      <svg
        className='checkbox sprite'
        style={ svgStyle }
        viewBox='0 0 14 14'>

        <path
          style={ checkboxStyle }
          d={ Checkbox.PATH }
        />

        { this.renderChecked(checked) }

      </svg>

      { this.props.children }

    </label>
  }

  // eslint-disable-next-line class-methods-use-this
  renderChecked (checked) {
    const checkedStyle = Object.assign({}, style.checkbox, {
      stroke: checked ? this.props.highlightColor : 'transparent',
    })

    return <g style={ checkedStyle }>
      <path d='m 4 5 l 5 5' />
      <path d='m 9 5 l -5 5' />
    </g>
  }

}

const style = {

  input: {
    display: 'none',
    opacity: 0,
    height:  0,
  },

  container: {
    maxHeight: 'none',
    maxWidth:  'none',
    display:   'block',
    cursor:    'pointer',
    height:    'auto',
    width:     'auto',
    margin:    0,
    padding:   0,
  },

  svg: {
    height:   'auto',
    display:  'block',
    maxWidth: 'none',
  },

  checkbox: {
    strokeWidth:    1.4,
    strokeLinejoin: 'round',
    strokeLinecap:  'round',
    vectorEffect:   'non-scaling-stroke',
    transition:     'stroke .2s',
    fill:           'transparent',
  },
}

const bool = (val) =>
  val ? true : false
