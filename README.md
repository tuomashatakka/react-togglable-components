## Installation

`npm i react-toggle-components --registry https://npm.fns.fi`


## Usage

```js

import React, { Component, Fragment } from 'react'
import { Checkbox, Radio } from 'react-toggle-components'

export class CheckboxField extends Component {
  state = { value: false }
  render = () => <Checkbox
    checked={ this.state.value }
    onChange={ () => this.setState({ value: !this.state.value })}
  />
}

export class RadioField extends Component {
  state = { value: false }
  render = () => <Fragment>
    <Radio
      name='radio-1'
      checked={ this.state.value === 1 }
      onChange={ () => this.setState({ value: 1 })}
    />
    <Radio
      name='radio-2'
      checked={ this.state.value === 2 }
      onChange={ () => this.setState({ value: 2 })}
    />
    <Radio
      name='radio-3'
      checked={ this.state.value === 3 }
      onChange={ () => this.setState({ value: 3 })}
    />
  </Fragment>
}

```
