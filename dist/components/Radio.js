'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  name: _propTypes2.default.string,
  value: _propTypes2.default.bool,
  checked: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  onChange: _propTypes2.default.func.isRequired,
  className: _propTypes2.default.string,

  width: _propTypes2.default.oneOf(['string', 'number']),
  color: _propTypes2.default.string,
  disabledColor: _propTypes2.default.string,
  highlightColor: _propTypes2.default.string
};

var defaultProps = {
  size: 1,
  value: false,
  color: 'currentColor',
  disabledColor: '#757082',
  highlightColor: '#4a4655'
};

var f1 = 2.76243;
var f2 = 2.23757;

var Radio = function (_PureComponent) {
  _inherits(Radio, _PureComponent);

  function Radio() {
    _classCallCheck(this, Radio);

    var _this = _possibleConstructorReturn(this, (Radio.__proto__ || Object.getPrototypeOf(Radio)).apply(this, arguments));

    _this.toggle = function () {
      return _this.props.onChange(!_this.checked);
    };

    (0, _assert2.default)(typeof _this.props.onChange === 'function', 'Checkbox must have an onChange prop of type function.');
    return _this;
  }

  _createClass(Radio, [{
    key: 'render',
    value: function render() {
      var disabled = this.disabled;
      var checked = this.checked;

      var color = this.props.color;
      var disabledColor = this.props.disabledColor;
      var highlightColor = this.props.highlightColor;

      var radioStyle = Object.assign({}, style.checkbox, {
        fill: !checked ? 'transparent' : disabled ? disabledColor : highlightColor,
        stroke: disabled ? disabledColor : checked ? highlightColor : color
      });

      var svgStyle = Object.assign({}, style.svg, {
        width: this.size
      });

      return _react2.default.createElement(
        'label',
        {
          style: style.container,
          className: this.props.className },
        _react2.default.createElement('input', {
          type: 'radio',
          name: this.props.name,
          style: style.input,
          checked: checked,
          disabled: disabled,
          onChange: this.toggle
        }),
        _react2.default.createElement(
          'svg',
          {
            className: 'checkbox sprite',
            style: svgStyle,
            viewBox: '0 0 14 14' },
          _react2.default.createElement('path', { style: radioStyle, d: Radio.PATH })
        )
      );
    }
  }, {
    key: 'checked',
    get: function get() {
      return bool('checked' in this.props ? this.props.checked : this.props.value);
    }
  }, {
    key: 'disabled',
    get: function get() {
      return this.props.disabled ? 'disabled' : false;
    }
  }, {
    key: 'size',
    get: function get() {
      if (typeof this.props.size === 'number') return this.props.size + 'em';

      // flow-ignore
      return this.props.size;
    }
  }]);

  return Radio;
}(_react.PureComponent);

Radio.PATH = 'm2,6.64029c0,-' + f1 + ' ' + f2 + ',-5 5,-5c' + f1 + ',0 5,' + f2 + ' 5,5c0,' + f1 + ' -' + f2 + ',5 -5,5c-' + f1 + ',0 -5,-' + f2 + ' -5,-5z';
Radio.propTypes = propTypes;
Radio.defaultProps = defaultProps;
exports.default = Radio;


var style = {

  input: {
    display: 'none',
    opacity: 0,
    height: 0
  },

  container: {
    maxHeight: 'none',
    maxWidth: 'none',
    display: 'block',
    cursor: 'pointer',
    height: 'auto',
    width: 'auto',
    margin: 0,
    padding: 0
  },

  svg: {
    height: 'auto',
    display: 'block',
    maxWidth: 'none'
  },

  checkbox: {
    strokeWidth: 1,
    strokeLinejoin: 'round',
    vectorEffect: 'non-scaling-stroke',
    transition: 'all .3s'
  }
};

var bool = function bool(val) {
  return val ? true : false;
};