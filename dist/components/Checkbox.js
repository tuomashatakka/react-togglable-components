'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var propTypes = {
  name: _propTypes2.default.string,
  value: _propTypes2.default.bool,
  checked: _propTypes2.default.bool,
  disabled: _propTypes2.default.bool,
  onChange: _propTypes2.default.func.isRequired,
  className: _propTypes2.default.string,
  children: _propTypes2.default.children,

  width: _propTypes2.default.oneOf(['string', 'number']),
  color: _propTypes2.default.string,
  highlightColor: _propTypes2.default.string
};

var defaultProps = {
  value: false,
  width: '1em',
  color: 'currentColor',
  highlightColor: '#4a4655'
};

var Checkbox = function (_PureComponent) {
  _inherits(Checkbox, _PureComponent);

  function Checkbox() {
    _classCallCheck(this, Checkbox);

    var _this = _possibleConstructorReturn(this, (Checkbox.__proto__ || Object.getPrototypeOf(Checkbox)).apply(this, arguments));

    _this.toggle = function () {
      return _this.props.onChange(!_this.checked);
    };

    (0, _assert2.default)(typeof _this.props.onChange === 'function', 'Checkbox must have an onChange prop of type function.');
    return _this;
  }

  _createClass(Checkbox, [{
    key: 'render',
    value: function render() {
      var color = this.props.color;
      var highlightColor = this.props.highlightColor;
      var checked = this.checked;

      var checkboxStyle = Object.assign({}, style.checkbox, {
        stroke: checked ? highlightColor : color
      });

      var svgStyle = Object.assign({}, style.svg, {
        width: this.props.width
      });

      return _react2.default.createElement(
        'label',
        {
          style: style.container,
          className: this.props.className },
        _react2.default.createElement('input', {
          type: 'checkbox',
          name: this.props.name,
          style: style.input,
          checked: checked,
          onChange: this.toggle
        }),
        _react2.default.createElement(
          'svg',
          {
            className: 'checkbox sprite',
            style: svgStyle,
            viewBox: '0 0 14 14' },
          _react2.default.createElement('path', {
            style: checkboxStyle,
            d: Checkbox.PATH
          }),
          this.renderChecked(checked)
        ),
        this.props.children
      );
    }

    // eslint-disable-next-line class-methods-use-this

  }, {
    key: 'renderChecked',
    value: function renderChecked(checked) {
      var checkedStyle = Object.assign({}, style.checkbox, {
        stroke: checked ? this.props.highlightColor : 'transparent'
      });

      return _react2.default.createElement(
        'g',
        { style: checkedStyle },
        _react2.default.createElement('path', { d: 'm 4 5 l 5 5' }),
        _react2.default.createElement('path', { d: 'm 9 5 l -5 5' })
      );
    }
  }, {
    key: 'checked',
    get: function get() {
      return bool('checked' in this.props ? this.props.checked : this.props.value);
    }
  }]);

  return Checkbox;
}(_react.PureComponent);

Checkbox.PATH = 'm 2 2 h 9 c 1 0 1 1 1 1 v 9 c 0 1 -1 1 -1 1 h -9 c -1 0 -1 -1 -1 -1 v -9 c 0 -1 1 -1 1 -1 z';
Checkbox.propTypes = propTypes;
Checkbox.defaultProps = defaultProps;
exports.default = Checkbox;


var style = {

  input: {
    display: 'none',
    opacity: 0,
    height: 0
  },

  container: {
    maxHeight: 'none',
    maxWidth: 'none',
    display: 'block',
    cursor: 'pointer',
    height: 'auto',
    width: 'auto',
    margin: 0,
    padding: 0
  },

  svg: {
    height: 'auto',
    display: 'block',
    maxWidth: 'none'
  },

  checkbox: {
    strokeWidth: 1.4,
    strokeLinejoin: 'round',
    strokeLinecap: 'round',
    vectorEffect: 'non-scaling-stroke',
    transition: 'stroke .2s',
    fill: 'transparent'
  }
};

var bool = function bool(val) {
  return val ? true : false;
};