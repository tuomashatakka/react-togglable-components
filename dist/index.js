'use strict';

var _Radio = require('./components/Radio');

var _Radio2 = _interopRequireDefault(_Radio);

var _Checkbox = require('./components/Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
  Radio: _Radio2.default,
  Checkbox: _Checkbox2.default
};